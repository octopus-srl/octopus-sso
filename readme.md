# Octopus SSO Client

Questa libreria fornisce un modo semplice e sicuro per autenticare le chiamate verso i microservizi che utilizzano l'autenticazione Oauth2 con Octopus SSO.
Per funzionare, bisogna che il client sia abilitato lato Octopus con un grant di tipo 'client_credentials' che invii il proprio bearer rilasciato da Octopus e la sua app name in un campo 'name' dell'header delle richieste

## Prerequisiti

- laravel 5.8
- PHP 7.1 o superiore

## Installation

Aggiungere nel composer.json

    "repositories": [
        {
          "type": "vcs",
          "url": "git@bitbucket.org:octopus_seregno/octopus-sso.git"
        }
      ]

Eseguire il comando:

    composer require octopus/sso.client
    
Per copiare il file di configurazione octopus-sso.php nella cartella config:

    php artisan vendor:publish --provider="Octopus\SsoClient\ServiceProvider"
    
Aggiungere nel file .env le seguenti variabili d'ambiente

    SSO_CLIENT_ID // Client id fornito da Octopus SSO
    SSO_CLIENT_SECRET // Client secret fornito da Octopus SSO
    SSO_BASE_URL // Base Url di Octopus SSO
    SSO_APP_NAME // Client Name fornito da Octopus SSO
    SSO_CLIENT_SCOPE // Scope fornito da Octopus SSO per la tua app
    
L'ultima cosa da fare è impostare un middleware 

Nel file /app/Http/Kernel.php aggiungere tra i $routeMiddleware:

    protected $routeMiddleware = [
            'auth'          => \App\Http\Middleware\Authenticate::class,
            ... altri middleware ...
            'validate.octopus.sso' => \Octopus\SsoClient\Middleware\ValidateSSO::class
        ];
    
e abilitarlo per quelle rotte che richiedono l'autenticazione da parte di Octopus SSO

    Route::group(['middleware' => 'validate.octopus.sso'], function () {
          ... elenco rotte da proteggere ...
        });