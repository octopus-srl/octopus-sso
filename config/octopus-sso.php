<?php

return [

    'base_url'     => env('SSO_BASE_URL', ''),
    'enable_cache' => env('SSO_ENABLE_CACHE', false),
    'client'       => [
        'grant_type'    => 'client_credentials',
        'client_id'     => env('SSO_CLIENT_ID', null),
        'client_secret' => env('SSO_CLIENT_SECRET', null),
        'scope'         => env('SSO_CLIENT_SCOPE', null),
    ],
];
