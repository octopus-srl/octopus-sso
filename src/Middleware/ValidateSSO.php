<?php

namespace Octopus\SsoClient\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Octopus\SsoClient\Clients\SsoClient;
use Octopus\SsoClient\SsoException;

class ValidateSSO
{
    /** @var SsoClient */
    private $client;

    public function __construct(SsoClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws SsoException
     */
    public function handle(Request $request, Closure $next)
    {

        if (!$token = $this->getToken($request)) {
            throw new SsoException('JWT Token missing', Response::HTTP_UNAUTHORIZED);
        }

        if (!$clientName = $request->header('client', null)) {
            throw new SsoException('Client name missing', Response::HTTP_UNAUTHORIZED);
        }

        /** check if response === $clientName */
        $clientInResponse = $this->client->validateRequest($token, $clientName);
        if($clientInResponse != $clientName) {
            throw new SsoException('Client name mismatch ' . $clientName . ' - ' . $clientInResponse, Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }

    public function getToken(Request $request)
    {
        $header = $request->headers->get('authorization', null);
        $prefix = 'bearer';

        if ($header && preg_match('/' . $prefix . '\s*(\S+)\b/i', $header, $matches)) {
            return $matches[1];
        }

        return null;
    }
}
