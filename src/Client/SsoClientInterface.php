<?php

namespace Octopus\SsoClient\Clients;

use Octopus\SsoClient\SsoException;

interface SsoClientInterface
{

    /**
     * @param string $jwt
     * @param string $client
     * @return mixed
     * @throws SsoException
     */
    public function validateRequest(string $jwt, string $client);


}
