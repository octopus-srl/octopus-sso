<?php

namespace Octopus\SsoClient\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;
use Octopus\SsoClient\SsoException;

class SsoClient implements SsoClientInterface
{

    /** @var Client */
    private $client;
    private $cacheKey;
    /**
     * SsoClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('octopus-sso.base_url'),
        ]);

        if (config('octopus-sso.enable_cache') && extension_loaded('apcu')) {
            $this->cacheKey = config('octopus-sso.client.client_secret');
        } else {
            $this->cacheKey = null;
        }


    }

    /**
     * @throws SsoException
     */
    private function setAccessToken()
    {
        $token = null;

        if ($this->cacheKey) {
            if (Cache::has($this->cacheKey)) {
                $token = Cache::get($this->cacheKey);
            }
        }

        if (empty($token)) {
            $token = $this->getNewAccessToken();
        }

        $this->client = new Client([
            'base_uri' => config('octopus-sso.base_url'),
            'headers'  => [
                'authorization' => 'Bearer ' . $token,
            ],
        ]);

    }

    /**
     * @return mixed
     * @throws SsoException
     */
    private function getNewAccessToken()
    {
        $jsonResponse = $this->client->post('api/v1/oauth/token',
            ['form_params' => config('octopus-sso.client')]);

        $response = json_decode((string)$jsonResponse->getBody(), true);

        if (empty($response['access_token'])) {
            throw new SsoException('Access token not received');
        }

        return $response['access_token'];

    }

    /**
     * @param string $jwt
     * @param string $client
     * @param bool $firstTime
     * @return array|mixed|null
     * @throws SsoException
     */
    public function validateRequest(string $jwt, string $client, bool $firstTime = true)
    {
        $this->setAccessToken();
        $response = $this->client->post('api/v1/validate-request',
            [
                'form_params' => [
                    'jwt'    => $jwt,
                    'client' => $client,
                ],
            ]);

        $code = $response->getStatusCode();
        $content = json_decode($response->getBody()->getContents(), true);

        switch ($code) {
            case '401':
                if(!$firstTime) {
                    throw new SsoException('Invalid Credentials');
                }
                $token = $this->getNewAccessToken();
                if ($this->cacheKey) {
                    Cache::forever($this->cacheKey, $token);
                }
                return $this->validateRequest($jwt, $client, false);
                break;
            case '200':
            case '201':
                return empty($content['client_name']) ? null : $content['client_name'];
            case '400':
            case '404':
                return null;
            default:
                return null;
        }

    }


}
